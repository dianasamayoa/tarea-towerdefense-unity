﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {
    [SerializeField] public GameObject levelsHolderBack;
    [SerializeField] public GameObject mainMenuBTS;
    
   
	void Start () {
       	
	}
    public void SelectLevelBT()
    {
       
        levelsHolderBack.SetActive(true);
        mainMenuBTS.SetActive(false);
    }
    public void QuitBT()
    {
        Application.Quit();
    }
    public void LoadLevelBT(string levelname)
    {

        SceneManager.LoadScene(levelname);
    }
   
}
