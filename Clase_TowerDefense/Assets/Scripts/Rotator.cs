﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    //this class is for the main title, rotation is simple
    [SerializeField] Vector3 rotationSpeed;

	// Update is called once per frame
	void Update () {
        // Modifies the parent transform in x, y and z
        transform.localEulerAngles += rotationSpeed;

    }
}
